CREATE DATABASE  IF NOT EXISTS `exapp` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `exapp`;
--
-- Table structure for table `bacons`
--

DROP TABLE IF EXISTS `bacons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bacons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(45) NOT NULL,
  `price` decimal(10,2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bacons`
--

LOCK TABLES `bacons` WRITE;
/*!40000 ALTER TABLE `bacons` DISABLE KEYS */;
INSERT INTO `bacons` VALUES (1,'chunky',24.5),(4,'slim',12.98),(5,'fatty',36.6),(6,'extra chunky',25),(7,'tender',25),(8,'smoky',25),(9,'striped',26.98),(10,'musky',25);
/*!40000 ALTER TABLE `bacons` ENABLE KEYS */;
UNLOCK TABLES;

